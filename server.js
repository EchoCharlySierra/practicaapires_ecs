var express = require('express');
var app = express();
var port = process.env.PORT || 3000;
// vamos a usar el framework

// para poder tratar el body
var bodyParser = require ('body-parser');
app.use(bodyParser.json());

// para el consumo de la parte de la API de MLAB, ponemos los trozos básicos de URL que habrá que repetir
var baseMLabURL = 'https://api.mlab.com/api/1/databases/apitechuecs/collections/';
var mLabAPIKey = "apiKey=rirjWZYm-pnViazTn2CIivJYQA-WZBl2";  // lo mejor sería tenerlo en un fichero properties
              // que no se pone en el control de versiones, pero bueno, podemos dejarlo así para el proyecto
              // si estuviese en el properties se podría más o menos así    apikey={{ENV}}
var requestJson = require('request-json');

app.listen(port); // estamos abriendo un canal lógico
console.log("API chachi piruli escuchando en el puerto " + port);

app.get("/apitechu/v1",      //nuestro punto de entrada
  function(req,res) {     // la función manejadora, con el request (lo que nos envía el navegador) y el response
                // esta funcion se ejecuta en modo callback, asincrono
    console.log("GET /apitechu/v1");
    res.send({"msg": "HOla desde apitechu"});
  }
)

app.get("/apitechu/v1/users",
  function (req,res){
    console.log("GET /apitechu/v1/users");
    //   res.sendFile('./usuarios.json');      //DEPRECATED
      res.sendFile('usuarios.json', {root: __dirname});
    //Otra manera:
    //var users=require('./usuarios.json');
    //res.send(users);
  }
)

app.post("/apitechu/v1/users",
  function(req,res) {
              console.log("POST  /apitechu/v1/users");
              //console.log(req.headers);
              //console.log(req.headers.first_name);
              console.log("first_name is " + req.body.first_name);
              console.log("last_name is " + req.body.last_name);
              console.log("country is " + req.body.country);

          var newUser = {
            "first_name" : req.body.first_name,
            "last_name" : req.body.last_name,
            "country" : req.body.country

          }
          var users=require('./usuarios.json');
          users.push(newUser);

          WriteUserDataToFile(users,'./usuarios.json');
          console.log("Usuario guardado con éxito");
          res.send({"msg":"Usuario guardado con éxito"});


  }

)

app.delete ("/apitechu/v1/users/:id",   // así indicamos que en la url vendrá un parámetro, con :id
                  // con lo que se invocaría así, por ej DELETE  http://localhost:3000/apitechu/V1/users/10
  function(req, res){
    console.log( "DELETE /apitechu/v1/users/:id");
    console.log(req.params);
    console.log(req.params.id);

    var users=require('./usuarios.json');
    users.splice(req.params.id - 1, 1);    // DEvuelve el array sin el trozo, desde el id-1 y quitando 1.
                  // ES POR POSICIÓN EN EL FICHERO, NO ES POR EL ID que tiene el elemento, es un ejemplo

    WriteUserDataToFile(users,'./usuarios.json');
    console.log ("Usuario borrado");
    res.send({"msg":"Usuario borrado"});

  }
)

//función común para escribir datos al fichero
function WriteUserDataToFile(data, file){

  var fs = require('fs');
  var jsonUserData = JSON.stringify(data);

  fs.writeFile(file,
    jsonUserData,
    "utf8",
    function(err){
      if (err){
        console.log(err);
      }else {
        console.log("Datos escritos en archivo");
      }
    }
  );
}


app.post("/apitechu/v1/monstruo/:p1/:p2",
  function (req,res) {
    console.log("Parámetros");
    console.log(req.params);

    console.log("Query String");
    console.log(req.query);

    console.log("Body");
    console.log(req.body);

    console.log("Headers");
    console.log(req.headers);

  }
)



app.post("/apitechu/v1/login",
  function(req,res) {
              console.log("POST  /apitechu/v1/login");


          var loginUser = {
            "email" : req.body.email,
            "password" : req.body.password
          }

          console.log ("En el body está el email: " + loginUser.email);
          console.log ("En el body está la password: " + loginUser.password);

          var fichero = './datos_practicaAPIRES_ECS.json';
          var users=require(fichero);

          var encontrado = false;

          var respuesta = {
            "mensaje" : "Login incorrecto"
          }


          for (user of users) {
            console.log(" leo usuario: " + user.id);
            console.log(" email es: " + user.email);

            if (user.email == loginUser.email)  {
              console.log("Encontrado el usuario " + user.email);
              if  (user.password == loginUser.password) {
                console.log("Clave ok " );
                respuesta.mensaje = "Login correcto";
                respuesta.id = user.id;
                //Añadimos la propiedad logged
                user.logged = true;
                encontrado = true;
                break;
              }
            }
            else {
              console.log("No es");
            }

          }



        if (encontrado == true) {

          WriteUserDataToFile(users,fichero);
          console.log("Usuario guardado con éxito");
        }
        else{
          console.log("Usuario no encontrado");
        }

        res.send(respuesta);

  }

)

app.post("/apitechu/v1/logout",
  function(req,res) {
              console.log("POST  /apitechu/v1/logout");


          var logoutUser = {
            "id" : req.body.id
          }

          console.log ("En el body está el id: " + logoutUser.id);

          var fichero = './datos_practicaAPIRES_ECS.json';
          var users=require(fichero);

          var encontrado = false;

          var respuesta = {
            "mensaje" : "Logout incorrecto"
          }


          for (user of users) {
            console.log(" leo usuario: " + user.id);
            if ((user.id == logoutUser.id)  && (user.logged == true)){
              console.log("Encontrado el usuario conectado" + user.id);
              respuesta.mensaje = "Logout correcto";
              respuesta.id = user.id;
                //Añadimos la propiedad logged
              delete  user.logged ;
              encontrado = true;
              break;

            }
            else {
              console.log("No es");
            }

          }



        if (encontrado == true) {

          WriteUserDataToFile(users,fichero);
          console.log("Usuario logout con éxito");
        }
        else{
          console.log("Usuario no estaba logado");
        }
          res.send(respuesta);


  }

)

// Ponemos versión 2 (v2), que es la que va a ir a buscar a mlab los usuarios
app.get("/apitechu/v2/users",
  function (req,res){
    console.log("GET /apitechu/v2/users");

    // la librería tiene un método createclient para hacer las peticiones
    // poniendo la base que hemos definido arriba
    var httpClient = requestJson.createClient(baseMLabURL);
    console.log("Cliente http creado");

    // A partir de aqui se hacen las peticiones:
    httpClient.get("user?" + mLabAPIKey,
      // tiene una función manejadora con los parámetros: error, response y body, donde estará el json devuelto
      // error 400 y pico => error en el cliente   y 500 y pico => error en el servidor
      function(err, resMLab, body){
        var response = !err ? body : {            // Si no hay error entonces devuelvo body, si no,  mensaje
          "msg" : "Error obteniendo usuarios"
        }
        res.send(response);
      }
    )
  }
);


// Query para obtener un usuario por id
app.get("/apitechu/v2/users/:id",
  function (req,res){
    console.log("GET /apitechu/v2/users/:id");

    // recojo el id que es un parámetro
    var id = req.params.id;

    // esto es lo que se usa para hacer una query en la API de Mlab
    var query = 'q={"id":' + id + '}'

    var httpClient = requestJson.createClient(baseMLabURL);
    console.log("Cliente http creado");

    // A partir de aqui se hacen las peticiones para la query, incluyendo el substring de la query y el &:
    httpClient.get("user?" + query + "&" + mLabAPIKey,
      // tiene una función manejadora con los parámetros: error, response y body, donde estará el json devuelto
      // error 400 y pico => error en el cliente   y 500 y pico => error en el servidor
      function(err, resMLab, body){
        // En vez de hacer lo que está comentado, vamos a tratar el estatus.
        //  var response = !err ? body : {            // Si no hay error entonces devuelvo body, si no,  mensaje
        //    "msg" : "Error obteniendo usuario."
        //  }

        var response = {};

        if (err){
           response =  {
            "msg" : "Error obteniendo usuario"
          }
          res.status(500);
        } else {
          if (body.length > 0){
            response = body;
          } else {
             response = {
               "msg" : "Usuario no encontrado"
             };
             res.status(404);
          }
        }
        res.send(response);
      }
    )
  }
);


// VERSION 2 de Login, en la que hacemos login pero buscando y actualizando sobre lo de MongoDB, lo que obtenemos
// mediante la API a MLAB
// Hacemos un put, no un post. Post es para crear y Put para modificar
app.put("/apitechu/v2/login",
  function(req,res) {
              console.log("PUT  /apitechu/v2/login");


          var loginUser = {
            "email" : req.body.email,
            "password" : req.body.password
          }

          console.log ("En el body está el email: " + loginUser.email);
          console.log ("En el body está la password: " + loginUser.password);


          // Para el caso de que lo encuentre el usuario, para poder ponerle el logged= true
          var putBody = '{"$set":{"logged":true}}';

          // esto es lo que se usa para hacer una query en la API de Mlab
          var query = 'q={"email":"' + loginUser.email + '", "password":"' + loginUser.password +'"}'

          var httpClient = requestJson.createClient(baseMLabURL);
          console.log("Cliente http creado");

          var response = {};

          // A partir de aqui se hacen las peticion para buscar ese usuario y clave, incluyendo el substring de la query y el &:
          httpClient.get("user?" + query + "&" + mLabAPIKey,
            // tiene una función manejadora con los parámetros: error, response y body, donde estará el json devuelto
            // error 400 y pico => error en el cliente   y 500 y pico => error en el servidor
            function(err, resMLab, body){

              if (err){
                 response =  {
                  "msg" : "Error obteniendo usuario"
                }
                res.status(500);
                res.send(response);
              } else {
                if (body.length > 0){
                  console.log("Encontrado el usuario " + loginUser.email);
console.log(body[0]);
                  //Vamos a escribir el campo logged=true en la base de datos
                  httpClient.put("user?" + query + "&" + mLabAPIKey , JSON.parse(putBody),
                    function (errEscritura,resMLabEscritura, bodyEscritura){
                      if (err){
                        res.status(505);
                        response =  {
                         "msg" : "Error escribiendo el logged=true"
                          }
                      } else {
                        if (bodyEscritura.lenght >0) {
                          console.log("Usuario " + loginUser.email + " conectado correctamente");
                          res.status(200);
                          response = body[0];  // Devolvemos el usuario que hemos encontrado
                        }else{
                          console.log("bodyEscritura vacío, pero:")
                          console.log("Usuario " + loginUser.email + " conectado correctamente");
                          res.status(200);
                          response = body[0];   // Devolvemos el usuario que hemos encontrado
  console.log("el body es " );
  console.log( body[0]);
                        }
                      }
console.log("antes send" );
console.log (response);
                      res.send(response);     // Hacemos el res.send al final de la función manejadora del put
                    }
                  );    // Fin del put
                } else {              // El body de la petición de get es vacío, no se ha encontrado usuario
                   response = {
                     "msg" : "Usuario no encontrado"
                   };
                   res.status(404);
                   res.send(response);
                }
              }
            }
          ) ;    // Fin del get

  }

);    // Fin del V2 login


// VERSION 2 de logout, en la que hacemos logout pero buscando y actualizando sobre lo de MongoDB, lo que obtenemos
// mediante la API a MLAB
// Hacemos un put, no un post. Post es para crear y Put para modificar
app.put("/apitechu/v2/logout",
  function(req,res) {
              console.log("PUT  /apitechu/v2/logout");


          var logoutUser = {
            "id" : req.body.id
          }

          console.log ("En el body está el id: " + logoutUser.id);


          // Para el caso de que lo encuentre el usuario, para poder borrar la propiedad logged
          var putBody = '{"$unset":{"logged":""}}';

          // esto es lo que se usa para hacer una query en la API de Mlab
          var query = 'q={"id": ' + logoutUser.id +'}'

          var httpClient = requestJson.createClient(baseMLabURL);
          console.log("Cliente http creado");

          var response = {};


          // A partir de aqui se hacen las peticion para buscar ese usuario y clave, incluyendo el substring de la query y el &:
          httpClient.get("user?" + query + "&" + mLabAPIKey,
            // tiene una función manejadora con los parámetros: error, response y body, donde estará el json devuelto
            // error 400 y pico => error en el cliente   y 500 y pico => error en el servidor
            function(err, resMLab, body){

              if (err){
                 res.status(500);
                 response =  {
                  "msg" : "Error obteniendo usuario"
                 }
                 res.send(response);
              } else {
                if (body.length > 0){
                  console.log("Encontrado el usuario " + logoutUser.id);
                  //Vamos a borrar el campo logged en la base de datos
                  httpClient.put("user?" + query + "&" + mLabAPIKey , JSON.parse(putBody),
                    function (errEscritura,resMLabEscritura, bodyEscritura){
                      if (err){
                        response =  {
                         "msg" : "Error borrando el logged"
                          }
                        res.status(505);
                      } else {
                        if (bodyEscritura.lenght >0) {
                          console.log("Usuario " + logoutUser.id + " desconectado correctamente");
                          res.status(200);
                          response = body[0];     // Devolvemos el usuario que hemos encontrado
                        } else {
                          console.log("bodyEscritura vacío, pero:")
                          console.log("Usuario " + logoutUser.id + " desconectado correctamente");
                          res.status(200);
                          response = body[0];     // Devolvemos el usuario que hemos encontrado

                        }
                      }
                      res.send(response);   // Hacemos el res.send al final de la función manejadora del put
                    }
                  );   // Final del put

                } else {              // El body de la petición de get es vacío, no se ha encontrado usuario
                   response = {
                     "msg" : "Usuario no encontrado"
                   };
                   res.status(404);
                   res.send(response);
                }
              }
            }
          )     // Fin del get

  }

);    // Fin del V2 logout
