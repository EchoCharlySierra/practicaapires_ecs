var mocha = require('mocha');
var chai = require('chai');
var chaihttp = require ('chai-http');

chai.use(chaihttp);

var should = chai.should();  //Es una interfaz FLUENT, Should have, should be, should...
                  // se van encadenando con el siguiente a través de punto.

var server = require('../server');

/* Como los test unitarios suelen ser muy numerosos, se agrupan por suits,
para que, por operatividad, sólol se lancen el suit sobre el que he tocado */

describe ('First test',     //Nos crea un suit
  function(){
    it('Test que el navegador funciona',     //CAda it es una prueba que se quiere hacer
      function (done){        // con done indicamos que esta prueba ha terminado, que puede probar las aserciones
        chai.request('http://www.duckduckgo.com')   //concatenamos con punto lo siguiente que queremos hacer
        .get('/')       // a partir del dominio base, probamos el get, para ver que esa dirección responde
        .end(
          function(err, res){         // El res contendrá la respuesta del get anterior
            console.log("Request has ended");
            console.log(err);
            //console.log(res);
            res.should.have.status(200); // asercion para ver que la petición se ha hecho correctamente.
            done();

          }
        )
      }
    );
  }
);

// HAcemos otra prueba unitaria, para ver que responde nuestra API, (ver en server.js la definición de get /apitechu/v1)
describe ('Test de API de usuarios TechU',     //Nos crea un suit
  function(){
    it('Prueba que la API de usuario funciona ok',     //CAda it es una prueba que se quiere hacer
      function (done){        // con done indicamos que esta prueba ha terminado, que puede probar las aserciones
        chai.request('http://localhost:3000')   //concatenamos con punto lo siguiente que queremos hacer
        .get('/apitechu/v1')       // a partir del dominio base, probamos el get, para ver que esa dirección responde
        .end(
          function(err, res){         // El res contendrá la respuesta del   get anterior
            console.log("Request has ended");
            //Hacemos 2 aserciones
            res.should.have.status(200); // asercion para ver que la petición se ha hecho correctamente.
            res.body.msg.should.be.eql("HOla desde apitechu");   // exactamente lo que habíamos puesto en server.js como salida si funciona en apitechu/v1
            done();

          }
        )
      }
    ),
    it('Prueba que la API devuelve una lista de usuarios correctos',     //CAda it es una prueba que se quiere hacer
      function (done){        // con done indicamos que esta prueba ha terminado, que puede probar las aserciones
        chai.request('http://localhost:3000')   //concatenamos con punto lo siguiente que queremos hacer
        .get('/apitechu/v1/users')       // a ver si devuelve bien la lista de usuarios
        .end(
          function(err, res){         // El res contendrá la respuesta del   get anterior
            console.log("Request has ended");
            //Hacemos 2 aserciones
            res.should.have.status(200); // asercion para ver que la petición se ha hecho correctamente.
            res.body.should.be.a("array");   // be.a   buscamos un tipo primitivo.  Debe devolver un array

            for (user of res.body){
              user.should.have.property('email');
              user.should.have.property('password');
            }

            done();

          }
        )
      }
    )
  }
);
